import cmath
import math
from fractions import Fraction
from math import pi
from timeit import timeit


def main():
    x = 1 / 3
    # x = Fraction(1, 3)
    for i in range(50):
        x = x * 4 - 1
        print(x)


# lebegőpontos számábrázolás, floating poin, float
# egész, integer
# tört Fraction
# binárisan kódolt decimális, BCD, bank
# komplex
# main()


x = 300
# print(f"{x} {x:x} {x:o} {x:b}")

print(1005/4)
print(1005//4)
print(divmod(1005, 4))
print(1005 % 4)

for x in range(1, 22):
    print(f"{x:2} {x:2x} {x:2o} {x:5b}")
    if x % 5 == 0:
        print("-------")

for x in range(124, 24000, 1567):
    print(f"--{x:06}--")
    # print(f"--{x:6<,}--")


pi
print(f'{pi:5.2f} {pi:5.3f} {100*pi:5.4e} normálalak')

print(100 * 6e23)
print(6e-23 / 2e-25)
print(6* 10**-23 / (2*10**-25))

#Komplex

print(1j**2)
print(cmath.sqrt(-4))   # square root
# print(math.sqrt(-4))   # ValueError