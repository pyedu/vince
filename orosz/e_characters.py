
file_name = 'zaliznjak.txt'

text0 = '''а#а
аба#аба',абы',абы',а'б,абе',аба'м,абу',абы',або'й,або'ю,аба'ми,абе',аба'х
абажур#абажу'р,абажу'ры,абажу'ра,абажу'ров,абажу'ру,абажу'рам,абажу'р,абажу'ры,абажу'ром,абажу'рами,абажу'ре,абажу'рах'''

def szavakat_kiszed(text):
    szavak = []
    sorok = text.splitlines()

    for sor in sorok:
        szo = sor.split('#')[0]
        # print(szo)
        szavak.append(szo)
    return szavak

# print(szavakat_kiszed(text0))

with open(file_name) as f:
    text = f.read()

szavak = szavakat_kiszed(text)
# print(szavak[:10])
# print(len(szavak))

e = "э"
e_darab = 0
for szo in szavak:
    if "э" in szo[1:]:
        # print(szo)
        e_darab += 1

print(e_darab)

e_eleju = 0
for szo in szavak:
    if "э" in szo[0]:
        # print(szo)
        e_eleju += 1

print(e_eleju)

e_eleju = 0
for szo in szavak:
    if "э" == szo[0]:
        # print(szo)
        e_eleju += 1

print(e_eleju)

e_eleju = 0
for szo in szavak:
    if szo.startswith("э"):
        # print(szo)
        e_eleju += 1

print(e_eleju)

