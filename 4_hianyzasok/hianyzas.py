with open("naplo.txt") as f:
    sorok = f.readlines()

hi = []
for sor in sorok:
    if sor[0] != '#':
        hi.append(sor)
print(f'A naplóban {len(hi)}')

# Ugyanezt csinálja, list comprehension néven kereshetsz rá
# Van dict és set comprehension is. Sokszor kényelmesebb és tömörebb mint a fenti.

hi2 = [len(l) for l in sorok if l[0] != '#']

print(f'A naplóban {len(hi2)}...')


for sor in sorok:
    if sor[0] == '#':
        _, honap, nap = sor.split()
    elif sor.strip() == '':
        continue
    else:
        csaladnev, keresztnev, hianyzas = sor.split()
        print(f"{csaladnev} {keresztnev} - {honap}-{nap} elso orajaban {hianyzas[0]}")

# else if helyett mindig lehet else és if, csak sok elif esetén "nagy lesz a behúzás"
# Ez egyezik a fentivel:
for sor in sorok:
    if sor[0] == '#':
        _, honap, nap = sor.split()
    else:
        if sor == '':
            continue
        else:
            csaladnev, keresztnev, hianyzas = sor.split()
            print(f"{csaladnev} {keresztnev} - {honap}-{nap} elso orajaban {hianyzas[0]}")

# Talán még egyszerűbb úgy, ha az elején splitelsz:

for sor in sorok:
    splitted = sor.split()
    if splitted[0] == '#':
        _, honap, nap = splitted
    elif len(splitted) == 0:  # üres sor
        continue
    else:
        csaladnev, keresztnev, hianyzas = splitted
        print(f"{csaladnev} {keresztnev} - {honap}-{nap} elso orajaban {hianyzas[0]}")
