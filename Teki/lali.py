"""
https://wiki.python.org/moin/HungarianPythonBooks
https://mtmi.unideb.hu/course/view.php?id=9
"""
import random
from time import sleep
from turtle import Turtle

lali = Turtle('turtle')
ili = Turtle('turtle')

# lali.forward(100)
# ili.right(90)
# ili.forward(100)
# ili.stamp()
# ili.forward(100)
colours = ["green", "red", "blue", "yellow", "orange", "yellow", "green", "blue", "indigo", "purple"]


def negyzet(teki, hossz):
    for i in range(4):
        teki.color(random.choice(colours))
        teki.forward(hossz)
        teki.left(90)


def negyzetsor(teki: Turtle, hossz: int, darab: int):
    for i in range(darab):
        negyzet(teki, hossz)
        teki.penup()
        teki.forward(hossz)
        teki.pendown()


negyzetsor(lali, 200, 3)


"""
negyzet(lali, 300)
ili.left(120)
negyzet(ili, 300)
"""

sleep(8)

